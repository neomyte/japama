# Japama

Japama is a Django website with three goals:
 * Allowing its development team to learn new things
 * Grouping miscellaneous projects in a single hub
 * More specifically serve as a tool for Japanese learning

# Projects

In Japama the various projects are divided into Django apps. Hence there is a one to one relationship between apps and projects. Still, if a project ends up being to important, it might be split into more than one app.  
The current projects are:
 * **antorsioa**: A project holding various information related to the unvierse of Àntôrsioa and Aras
 * **common**: A project bearing common tools for other projects as well as the hub to access them
 * **kanas**: A simple training projects that shows the different katakanas and hiraganas
 * **kata**: Main project with different learning tools for japanese sessions

# Deployment

The project follows a trunk based flow.  
  
This project possess two environment:
 * *dev*: the last development version
 * *prod*: the production environment

A release branch is defined by the following syntax: <ins>vX.x</ins> (X is major version, y is minor version).  
  
As of right now every pushes on a release branch will update the production and every other pushes will update the development environment. After the release of 1.0, the development environment will only be update when master is pushed.

# Team

 * **Mrassoss**: Maintainer
 * **Orijjin**: Maintainer