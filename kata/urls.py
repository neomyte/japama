from django.urls import (path, re_path)
from django.conf.urls import include

from .views import Katas, Register
from .views.globe import GlobeView, Create, QuestionView, ResultsView, UserResultsView, Activate, Edit
from .views.katatoeba import ExampleView, NewExample, Validate, EditExample, EditSingleExample

common_patterns = [
    path('',          Katas.as_view(),    name='katas'    ),
    path('register/', Register.as_view(), name='register' ),
]

globe_patterns = [
    path    ('globe/',                                              GlobeView.as_view(),       name='globe'     ),
    path    ('globe/create/',                                       Create.as_view(),          name='create'    ),
    path    ('globe/activate/',                                     Activate.as_view(),        name='activate'  ),
    re_path ('globe/(?P<miniglobe>\w+)/$',                          QuestionView.as_view(),    name='questions' ),
    re_path ('globe/(?P<miniglobe>\w+)/edit/$',                     Edit.as_view(),            name='edit'      ),
    re_path ('globe/(?P<miniglobe>\w+)/results/$',                  ResultsView.as_view(),     name='results'   ),
    re_path ('globe/(?P<miniglobe>\w+)/results/(?P<user_id>\w+)/$', UserResultsView.as_view(), name='uresults'  ),
]

katatoeba_patterns = [
    path   ('katatoeba/',                   ExampleView.as_view(),       name='katatoeba'          ),
    path   ('katatoeba/create/',            NewExample.as_view(),        name='createexample'      ),
    path   ('katatoeba/validate/',          Validate.as_view(),          name='validate'           ),
    path   ('katatoeba/edit/',              EditExample.as_view(),       name='editexample'        ),
    re_path('katatoeba/edit/(?P<id>\w+)/$', EditSingleExample.as_view(), name='editsingleexample'  ),
]
urlpatterns = common_patterns + globe_patterns + katatoeba_patterns