from django.shortcuts import render
from django.http import Http404

# All keys MUST BE UNIQUE!
# This tree is used to generate crumbs
TREE = {
    'Home': {
        'Katas': {
            'G.L.O.B.E.': {
                '_globe_': {
                    'Results': {
                        '_result_user_': {}
                    },
                    'Edit globe': {}
                },
                'Create globe': {},
                'Activate globes': {}
            },
            'Katatoeba': {
                'Create example': {},
                'Edit examples': {},
                'Validate examples': {
                    '_edit_example_': {}
                }
            }
        }
    }
}

def path_to_key(leaf, path=[], tree=TREE):
    for key, _ in tree.items():
        if(key == leaf):
            return { 'found': True, 'path': path }
    for key, next in tree.items():
        res = path_to_key(leaf, path+[key], next)
        if(res['found']):
            return res
    return { 'found': False, 'path': path }

def render_page(request, template, title, context, vars={}):
    path_finder = path_to_key(title)
    if(path_finder['found']):
        return render(request, template, { 'title': title if title not in vars else vars[title], 'crumbs': crumbs(path_finder['path']+[title], vars), **context })
    else:
        raise Http404('Page not found')

def crumbs(crumbs, vars):
    redirs = ['../'*(len(crumbs)-n-1) if n != len(crumbs)-1 else './' for n in range(len(crumbs))]
    return { (el[0] if el[0] not in vars else vars[el[0]]): el[1] for el in zip(crumbs, redirs) }