from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.views import View

class Register(View):
    def get(self, request):
        return render(request, 'register.html', { 'form': UserCreationForm() })

    def post(self, request):
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            return redirect('/')

        return render(request, 'register.html', { 'form': form })