from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View

from ...models import Example
from ..utils import render_page

import json, re

class NewExample(LoginRequiredMixin, View):
    def get(self, request):
        return render_page(request, 'katatoeba/create.html', 'Create example', {})
    def post(self, request):
        data = request.POST.dict()
        Example.objects.create(
            french = data['french'],
            japanese = data['japanese'],
            tags = json.dumps(re.split(' *, *', data['tags'].lower()), ensure_ascii=False),
            validated = False,
            user = request.user
        )
        return redirect('/katas/katatoeba/')