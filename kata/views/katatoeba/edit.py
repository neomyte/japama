from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, redirect
from django.views import View

from ...models import Example
from ..utils import render_page

import json, re

def get_editables(user):
    candidates = Example.objects.filter(validated=False)
    return candidates if user.has_perm('kata.change_example') else candidates.filter(user=user)


class EditExample(LoginRequiredMixin, View):
    def get(self, request):
        examples = get_editables(request.user)
        for example in examples:
            example.tags = ', '.join(json.loads(example.tags))
        return render_page(request, 'katatoeba/edit.html', 'Edit examples', { 'examples': examples })
    def post(self, request):
        data = request.POST.dict()
        for example in get_editables(request.user):
            example.french   = data[f'french {example.id}']
            example.japanese = data[f'japanese {example.id}']
            example.tags = json.dumps(re.split(' *, *', data[f'tags {example.id}'].lower()), ensure_ascii=False)
            example.save()
        return redirect('/katas/katatoeba/validate/')


class EditSingleExample(PermissionRequiredMixin, View):
    permission_required = 'kata.change_example'
    def get(self, request, id):
        example = Example.objects.get(id=id)
        example.tags = ', '.join(json.loads(example.tags))
        return render_page(request, 'katatoeba/edit.html', '_edit_example_', { 'examples': [example] }, { '_edit_example_': id })
    def post(self, request, id):
        data = request.POST.dict()
        example = Example.objects.get(id=id)
        example.french   = data[f'french {id}']
        example.japanese = data[f'japanese {id}']
        example.tags = json.dumps(re.split(' *, *', data[f'tags {id}'].lower()), ensure_ascii=False)
        example.save()
        return redirect('/katas/katatoeba/validate/')