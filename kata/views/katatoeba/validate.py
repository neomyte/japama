from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render, redirect
from django.views import View

from ...models import Example, Tag
from ..utils import render_page

import json

class Validate(PermissionRequiredMixin, View):
    permission_required = 'kata.change_example'
    def get(self, request):
        return render_page(request, 'katatoeba/validate.html', 'Validate examples', { 'examples': Example.objects.filter(validated=False) })
    def post(self, request):
        data = request.POST.dict()
        goners = data['goners'].split(',') if data['goners'] != '' else []
        valids = data['valids'].split(',') if data['valids'] != '' else []
        for goner in goners:
            example = Example.objects.get(id=goner)
            example.delete()
        for valid in valids:
            example = Example.objects.get(id=valid)
            example.validated = True
            example.save()
            for tag in json.loads(example.tags):
                try:
                    Tag.objects.create(name=tag)
                except:
                    pass
                tag_object = Tag.objects.get(name=tag)
                tag_object.examples.add(example)
                tag_object.save()
        for eid, comment in json.loads(data['comments']).items():
            example = Example.objects.get(id=eid)
            example.comment = comment
            example.save()
        return redirect('/katas/katatoeba/')