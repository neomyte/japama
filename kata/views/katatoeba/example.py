from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import View

from ...models import Example, Tag
from ..utils import render_page

import re

class ExampleView(LoginRequiredMixin, View):
    def get(self, request):
        context = {
            'examples': Example.objects.filter(validated=True),
            'tags': Tag.objects.all(),
            'ctags': [],
            'type': 'and',
            'lang': 'JP'
        }
        return render_page(request, 'katatoeba/example.html', 'Katatoeba', context)
    def post(self, request):
        data = request.POST.dict()
        try:
            tags = request.POST.getlist('tags')
            if(data['type'] == 'or'):
                examples = set(Example.objects.filter(tag__name__in=tags))
            else:
                examples = Example.objects.all()
                for tag in tags:
                    examples = examples.filter(tag__name=tag)
        except:
            tags = []
            examples = Example.objects.all()
        context = {
            'examples': examples,
            'tags': Tag.objects.all(),
            'ctags': tags,
            'type': data['type'],
            'lang': data['lang']
        }
        return render_page(request, 'katatoeba/example.html', 'Katatoeba', context)