from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View

from ...models import Globe, Question, DIFFS, Resolution
from ..utils import render_page
from .create import randstr, correct_answer, answers

import random, json, string

def get_answers(questions):
    return [
        [
            [ answer == question.answer, answer ]
            for answer in [question.answer1, question.answer2, question.answer3, question.answer4, question.answer5, question.answer6]
        ]
        if question.qtype == 'SC' else
        [
            [ answer in json.loads(question.answer), answer ]
            for answer in [question.answer1, question.answer2, question.answer3, question.answer4, question.answer5, question.answer6]
        ]
        if question.qtype == 'MC' else
        [
            [ True, question.answer ]
        ]
        for question in questions
    ]

class Edit(LoginRequiredMixin, View):
    def get(self, request, miniglobe):
        globe = Globe.objects.get(mini=miniglobe)
        questions = Question.objects.filter(globe=globe)
        context = {
            'fdps': range(6),
            'questions': questions,
            'globe': globe,
            'qs': ','.join([str(n) for n in range(len(questions))]),
            'diffs': DIFFS,
            'answers': get_answers(questions)
        }
        return render_page(request, 'globe/edit.html', 'Edit globe', context, { '_globe_': globe.theme })
    def post(self, request, miniglobe):
        data = request.POST.dict()
        globe = Globe.objects.get(mini=miniglobe)
        qs = data['qs'].split(',')
        remaining_questions = []
        for idx in range(0, len(qs)):
            i = qs[idx]
            state = data[f'state{i}']
            if(state == 'new'):
                question = Question.objects.create(
                    globe = globe,
                    text = data[f'text{i}'],
                    diff = data[f'diff{i}'],
                    qtype = data[f'type{i}'],
                    answer = correct_answer(request, data, i),
                    **answers(data, i)
                )
                remaining_questions.append(question.id)
            else:
                qid = int(state)
                remaining_questions.append(qid)
                question = Question.objects.get(id=qid)
                question.text = data[f'text{i}']
                question.diff = data[f'diff{i}']
                question.qtype = data[f'type{i}']
                question.answer = correct_answer(request, data, i)
                ans = answers(data, i)
                question.answer1 = ans['answer1']
                question.answer2 = ans['answer2']
                question.answer3 = ans['answer3']
                question.answer4 = ans['answer4']
                question.answer5 = ans['answer5']
                question.answer6 = ans['answer6']
                question.save()
        for question in Question.objects.exclude(id__in=remaining_questions):
            question.delete()
        for resolution in Resolution.objects.filter(globe=globe):
            resolution.delete()
        return redirect('../')