from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import View

from ...models import Globe, Question
from ..utils import render_page

import random, json, string

def randstr(stringLength=30):
    return ''.join((random.choice(string.ascii_letters + string.digits) for i in range(stringLength)))

def correct_answer(request, data, i):
    qtype = data[f'type{i}']
    if(qtype == 'SC'):
        num = data[f'ans{i}']
        return data[f'ans{i}_{num}']
    elif(qtype == 'MC'):
        return json.dumps([ data[f'ans{i}_{num}'] for num in request.POST.getlist(f'ans{i}') ], ensure_ascii=False)
    elif(qtype == 'TA'):
        return data[f'ans{i}']

def answers(data, i):
    return { f'answer{n}': data[f'ans{i}_{n}'] if data[f'type{i}'] != 'TA' else '' for n in range(1, 7) }

class Create(LoginRequiredMixin, View):
    def get(self, request):
        return render_page(request, 'globe/create.html', 'Create globe', {})
    def post(self, request):
        data = request.POST.dict()
        globe = Globe.objects.create(
            user = request.user,
            theme = data['globe'],
            mini = randstr()
        )
        qs = data['qs'].split(';')
        for idx in range(0, len(qs)):
            i = qs[idx]
            Question.objects.create(
                globe = globe,
                text = data[f'text{i}'],
                diff = data[f'diff{i}'],
                qtype = data[f'type{i}'],
                answer = correct_answer(request, data, i),
                **answers(data, i)
            )
        return redirect('/katas/globe/')