from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect
from django.views import View

from ...models import Globe
from ..utils import render_page

def set_to(ids, value, user):
    admin = user.has_perm('kata.change_globe')
    for id in ids:
        globe = Globe.objects.get(id=id)
        if(admin or globe.user == user):
            globe.activated = value
            globe.save()
        else:
            raise PermissionDenied

class Activate(LoginRequiredMixin, View):
    def get(self, request):
        globes = Globe.objects.all() if request.user.has_perm('kata.change_globe') else Globe.objects.filter(user=request.user)
        return render_page(request, 'globe/activate.html', 'Activate globes', { 'globes': globes })
    def post(self, request):
        data = request.POST.dict()
        goners = data['goners'].split(',') if data['goners'] != '' else []
        valids = data['valids'].split(',') if data['valids'] != '' else []
        set_to(goners, False, request.user)
        set_to(valids, True, request.user)
        return redirect('/katas/globe/')