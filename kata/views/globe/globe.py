from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import View

from ...models import Globe
from ..utils import render_page


class GlobeView(LoginRequiredMixin, View):
    def get(self, request):
        return render_page(request, 'globe/globe.html', 'G.L.O.B.E.', { 'globes': Globe.objects.all() })