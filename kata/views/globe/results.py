from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.shortcuts import render
from django.views import View

from ...models import Globe, Resolution, Question
from ..utils import render_page
from .utils import can_edit

import random, json

class ResultsView(LoginRequiredMixin, View):
    def get(self, request, miniglobe):
        globe = Globe.objects.get(mini=miniglobe)
        if(not can_see_results(request, globe)):
            raise PermissionDenied
        resolutions = Resolution.objects.filter(globe=globe)
        return render_page(request, 'globe/results.html', 'Results', { 'resolutions': resolutions }, { '_globe_': globe.theme })

class UserResultsView(LoginRequiredMixin, View):
    def get(self, request, miniglobe, user_id):
        globe = Globe.objects.get(mini=miniglobe)
        user = User.objects.get(id=user_id)
        if(not can_see_results(request, globe)):
            raise PermissionDenied
        return results(request, miniglobe, user, '_result_user_', { '_result_user_': f'{user.first_name} {user.last_name} ({user.username})' })

def can_see_results(request, globe):
    return globe.user == request.user or request.user.has_perm('kata.view_globe')

def results(request, miniglobe):
    return results(request, miniglobe, request.user)

def results(request, miniglobe, user, leaf='_globe_', addons={}):
    globe = Globe.objects.get(mini=miniglobe)
    try:
        resolution = Resolution.objects.get(user=user, globe=globe)
        answers = json.loads(resolution.answers)
    except ObjectDoesNotExist:
        answers = {}
    return get_result(request, globe, answers, leaf, addons)

def get_result(request, globe, answers, leaf='_globe_', addons={}):
    def result_answer(question):
        id = str(question.id)
        present = id in answers
        return {
            'result': question.answer == answers[id] if question.qtype != 'MC'\
                                                     else set(json.loads(question.answer)) == set(answers[id])\
                                                     if present else False,
            'answer': answers[id] if present else 'Did not answer'
        }
    questions = [
        {
            'text': question.text,
            **get_answer(question),
            **result_answer(question)
        }
        for question in Question.objects.filter(globe=globe)
    ]
    goods = [ q['result'] for q in questions ].count(True)

    context = {
        'can_view_results': can_see_results(request, globe),
        'can_edit': can_edit(request, globe), 
        'theme': globe.theme,
        'questions': questions,
        'goods': goods,
        'out_of': len(questions)
    }
    return render_page(request, 'globe/result.html', leaf, context, { '_globe_': globe.theme, **addons })

def get_answer(question):
    correct_answer = question.answer
    answer_or_none = lambda answer: answer if answer else None
    possible_answers = [
        answer_or_none(question.answer1),
        answer_or_none(question.answer2),
        answer_or_none(question.answer3),
        answer_or_none(question.answer4),
        answer_or_none(question.answer5),
        answer_or_none(question.answer6),
    ]
    possible_answers = [ possible_answer for possible_answer in possible_answers if possible_answer ]
    random.shuffle(possible_answers)
    return { 'correct': correct_answer, 'choices': possible_answers }