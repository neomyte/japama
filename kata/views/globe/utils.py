def can_edit(request, globe):
    return request.user.has_perm('kata.change_globe') or globe.user == request.user