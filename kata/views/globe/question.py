from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.shortcuts import render
from django.views import View

from ...models import Globe, Resolution, Question
from ..utils import render_page
from .results import get_result, get_answer
from .utils import can_edit

import json, random


DIFFICULTY_MAPPER = {
    'BA': { 'name': 'Basics', 'color': 'pink' },
    'EZ': { 'name': 'Easy', 'color': 'green' },
    'IM': { 'name': 'Intermediary', 'color': 'blue' },
    'DF': { 'name': 'Difficult', 'color': 'orange' },
    'MR': { 'name': 'Muri', 'color': 'red' },
    'MD': { 'name': 'Muda', 'color': 'black' }
}

class QuestionView(LoginRequiredMixin, View):
    def get(self, request, miniglobe):
        globe = Globe.objects.get(mini=miniglobe)
        if(not globe.activated and not request.user.has_perm('kata.change_globe')):
            raise PermissionDenied
        try:
            resolution = Resolution.objects.get(user=request.user, globe=globe)
            answers = json.loads(resolution.answers)
            return get_result(request, globe, answers)
        except ObjectDoesNotExist:
            pass
        questions = [
            {
                'diff': DIFFICULTY_MAPPER[question.diff],
                'desc': question,
                'answer': get_answer(question)
            }
            for question in Question.objects.filter(globe=globe)
        ]
        context = {
            'globe': globe,
            'questions': questions,
            'can_edit': can_edit(request, globe)
        }
        return render_page(request, 'globe/question.html', '_globe_', context, { '_globe_': globe.theme })
    
    def post(self, request, miniglobe):
        globe = Globe.objects.get(mini=miniglobe)
        if(not globe.activated and not request.user.has_perm('kata.change_globe')):
            raise PermissionDenied
        mc_answers = {
            key.replace('answersTo ',''): request.POST.getlist(key)
            for key, value in request.POST.items()
            if 'answersTo ' in key
        }
        ot_answers = {
            key.replace('answerTo ',''): value
            for key, value in request.POST.items()
            if 'answerTo ' in key
        }
        answers = { **mc_answers, **ot_answers }
        try:
            resolution = Resolution.objects.get(user=request.user, globe=globe)
            answers = json.loads(resolution.answers)
        except ObjectDoesNotExist:
            Resolution.objects.create(
                user = request.user,
                globe = globe,
                answers = json.dumps(answers, ensure_ascii=False)
            )
        return get_result(request, globe, answers)