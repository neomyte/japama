from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import View

from .utils import render_page

class Katas(LoginRequiredMixin, View):
    def get(self, request):
        return render_page(request, 'katas.html', 'Katas', {})