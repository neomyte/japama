from django import template
from ..models import DIFFS

register = template.Library()

@register.filter
def item(dict, key):
    return dict[key]


@register.filter
def text(diff):
    eq = { e['value']: e['text'] for e in DIFFS }
    return eq[diff]