# Generated by Django 3.0.7 on 2020-06-24 19:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('kata', '0007_auto_20200619_2029'),
    ]

    operations = [
        migrations.AddField(
            model_name='resolution',
            name='question',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='kata.Question'),
            preserve_default=False,
        ),
    ]
