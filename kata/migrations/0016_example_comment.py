# Generated by Django 3.0.7 on 2020-07-18 20:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kata', '0015_example_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='example',
            name='comment',
            field=models.CharField(default='', max_length=3000),
        ),
    ]
