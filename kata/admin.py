from django.contrib import admin

from .models import Globe, Resolution, Question, Example, Tag

admin.site.register(Globe)
admin.site.register(Resolution)
admin.site.register(Question)

admin.site.register(Example)
admin.site.register(Tag)