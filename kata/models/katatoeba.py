from django.contrib.auth.models import User
from django.db import models

class Example(models.Model):
    french = models.CharField(max_length=1000)
    japanese = models.CharField(max_length=1000)
    tags = models.CharField(max_length=3000)
    validated = models.BooleanField(default=True)
    user = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    comment = models.CharField(max_length=3000, default='')
    def __str__(self):
        return self.french

class Tag(models.Model):
    name = models.CharField(max_length=50, unique=True)
    examples = models.ManyToManyField(Example)
    def __str__(self):
        return self.name