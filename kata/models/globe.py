from django.db import models
from django.contrib.auth.models import User

DIFFS = [
    { 'value': 'BA', 'text': 'Basics' },
    { 'value': 'EZ', 'text': 'Easy' },
    { 'value': 'IM', 'text': 'Intermediary' },
    { 'value': 'DF', 'text': 'Difficult' },
    { 'value': 'MR', 'text': 'Muri' },
    { 'value': 'MD', 'text': 'Muda' },
]

class Globe(models.Model):
    theme = models.CharField(max_length=1000)
    mini = models.CharField(max_length=30)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    doc = models.DateField(auto_now_add=True)
    activated = models.BooleanField(default=False)
    def __str__(self):
        return self.theme

class Question(models.Model):
    text = models.CharField(max_length=1000)
    qtype = models.CharField(
        max_length=2,
        choices= [
            ('MC', 'Multi Choice'),
            ('SC', 'Single Choice'),
            ('TA', 'Text Answer'),
        ],
        default='MC',
    )
    diff = models.CharField(
        max_length=2,
        choices= [ (diff['value'], diff['text']) for diff in DIFFS ],
        default='EZ',
    )
    answer  = models.CharField(max_length=300, default='', blank=True)
    answer1 = models.CharField(max_length=300, default='', blank=True)
    answer2 = models.CharField(max_length=300, default='', blank=True)
    answer3 = models.CharField(max_length=300, default='', blank=True)
    answer4 = models.CharField(max_length=300, default='', blank=True)
    answer5 = models.CharField(max_length=300, default='', blank=True)
    answer6 = models.CharField(max_length=300, default='', blank=True)
    globe = models.ForeignKey(
        Globe,
        on_delete=models.CASCADE
    )
    def __str__(self):
        return self.text

class Resolution(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    dor = models.DateField(auto_now_add=True)
    answers = models.CharField(max_length=3000)
    globe = models.ForeignKey(
        Globe,
        on_delete=models.CASCADE
    )
    def __str__(self):
        return f'{str(self.user)}_{self.globe.theme}'