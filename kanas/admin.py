from django.contrib import admin

from .models import Kana

admin.site.register(Kana)