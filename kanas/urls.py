from django.urls import (path, re_path)

from . import views

urlpatterns = [
    path('katakanas', views.katakana, name='katakana'),
    path('hiraganas', views.hiragana, name='hiragana'),
    re_path('katakanas/(?P<vowel>[aoueiAOUEI])/', views.katakana_by_vowel, name='katakana_vowel'),
    re_path('katakanas/(?P<consonant>[nwrymhtskNWRYMHTSK])/', views.katakana_by_consonant, name='katakana_consonant'),
    re_path('hiraganas/(?P<vowel>[aoueiAOUEI])/', views.hiragana_by_vowel, name='hiragana_vowel'),
    re_path('hiraganas/(?P<consonant>[nwrymhtskNWRYMHTSK])/', views.hiragana_by_consonant, name='hiragana_consonant'),
]