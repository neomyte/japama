# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse

from .models import Kana

HIRAGANA = 0
KATAKANA = 1

def to_kana(kana, type):
    katakanas = {
        'a': 'ア', 'i': 'イ', 'u': 'ウ', 'e': 'エ', 'o': 'オ',
        'ta': 'タ', 'chi': 'チ', 'tsu': 'ツ', 'te': 'テ', 'to': 'ト',
        'ka': 'カ', 'ki': 'キ', 'ku': 'ク', 'ke': 'ケ', 'ko': 'コ',
        'sa': 'サ', 'shi': 'シ', 'su': 'ス', 'se': 'セ', 'so': 'ソ',
        'na': 'ナ', 'ni': 'ニ', 'nu': 'ヌ', 'ne': 'ネ', 'no': 'ノ',
        'ha': 'ハ', 'hi': 'ヒ', 'fu': 'フ', 'he': 'ヘ', 'ho': 'ホ',
        'ma': 'マ', 'mi': 'ミ', 'mu': 'ム', 'me': 'メ', 'mo': 'モ',
        'ya': 'ヤ', 'yu': 'ユ', 'yo': 'ヨ',
        'ra': 'ラ', 'ri': 'リ', 'ru': 'ル', 're': 'レ', 'ro': 'ロ',
        'wa': 'ワ', 'wo': 'ヲ',
        'n': 'ン',
    }
    hiraganas = {
        'a': 'あ', 'i': 'い', 'u': 'う', 'e': 'え', 'o': 'お',
        'ta': 'た', 'chi': 'ち', 'tsu': 'つ', 'te': 'て', 'to': 'と',
        'ka': 'か', 'ki': 'き', 'ku': 'く', 'ke': 'け', 'ko': 'こ',
        'sa': 'さ', 'shi': 'し', 'su': 'す', 'se': 'せ', 'so': 'そ',
        'na': 'な', 'ni': 'に', 'nu': 'ぬ', 'ne': 'ね', 'no': 'の',
        'ha': 'は', 'hi': 'ひ', 'fu': 'ふ', 'he': 'へ', 'ho': 'ほ',
        'ma': 'ま', 'mi': 'み', 'mu': 'む', 'me': 'め', 'mo': 'も',
        'ya': 'や', 'yu': 'ゆ', 'yo': 'よ',
        'ra': 'ら', 'ri': 'り', 'ru': 'る', 're': 'れ', 'ro': 'ろ',
        'wa': 'わ', 'wo': 'を',
        'n': 'ん',
    }
    return hiraganas[kana] if type == HIRAGANA else katakanas[kana]


def hiragana(request):
    return list(request, HIRAGANA)
def katakana(request):
    return list(request, KATAKANA)

def hiragana_by_vowel(request, vowel):
    return by_vowel(request, vowel, HIRAGANA)
def katakana_by_vowel(request, vowel):
    return by_vowel(request, vowel, KATAKANA)

def hiragana_by_consonant(request, consonant):
    return by_consonant(request, consonant, HIRAGANA)
def katakana_by_consonant(request, consonant):
    return by_consonant(request, consonant, KATAKANA)

def by_vowel(request, vowel, type):
    consonants = [ kana.name for kana in Kana.objects.filter(vowel=vowel) ]
    return generic_by(request, consonants, type)
def by_consonant(request, consonant, type):
    consonants = [ kana.name for kana in Kana.objects.filter(consonant=consonant) ]
    return generic_by(request, consonants, type)
def list(request, type):
    all = [ kana.name for kana in Kana.objects.all() ]
    return generic_by(request, all, type)

def generic_by(request, kanas, type):
    template = loader.get_template('kanas.html')
    type_name = 'Katakana' if type == KATAKANA else 'Hiragana'
    context = {
        'kanas': [ 
            { 
                'roumaji': kana, 
                'kana': to_kana(kana, type),
            }
            for kana in kanas
        ],
        'type': type_name,
        'title': f'{type_name} List',
        'crumbs': { 'Home': '../../', type_name: type_name.lower()+'s' }
    }
    return HttpResponse(template.render(context, request))
