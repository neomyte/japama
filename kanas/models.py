from django.db import models

class Kana(models.Model):
    name = models.CharField(max_length=3)
    vowel = models.CharField(max_length=1)
    consonant = models.CharField(max_length=1)
    # s -> s/sh
    # t -> t/tch/ts
    def is_consonant(self, consonant):
        return self.consonant == consonant
    def is_vowel(self, vowel):
        return self.vowel == vowel
    def __str__(self):
        return self.name