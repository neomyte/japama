FROM django
ADD . /
EXPOSE 8000
ENV DB_PATH /var/db.sqlite3
CMD python manage.py runserver 0:8000 --noreload
