from django.shortcuts import render

def index(request):
    context = {
        'title': 'Welcome to Japama',
        'buttons': [
            { 'href': 'kanas/hiraganas',    'title': '平仮名 (Hiragana)' },
            { 'href': 'kanas/katakanas',    'title': '片仮名 (Katakana)' },
            { 'href': 'ages',               'title': 'Àntôrsioa'        },
            { 'href': 'admin',              'title': 'Administration'   },
            { 'href': 'katas',              'title': '型'               },
            { 'href': 'http://zigip.hopto.org:12378', 'title': 'Generate Anki deck' },
            { 'href': 'accounts/logout',    'title': 'Log out'          },
        ],
        'crumbs': {'Home': './'}
    }
    return render(request, 'index.html', context)
