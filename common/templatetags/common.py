from django import template
from django.template.defaultfilters import stringfilter
import random


register = template.Library()

@register.filter
@stringfilter
def no_quote(value):
    return value.replace("'", '\\\'')

@register.filter
def shuffle(seq):
    try:
        result = list(seq)
        random.shuffle(result)
        return result
    except:
        return seq