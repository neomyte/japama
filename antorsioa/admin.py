from django.contrib import admin

from .models import (Age, Time, Major)

admin.site.register(Age)
admin.site.register(Time)
admin.site.register(Major)