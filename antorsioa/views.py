# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist

from .models import (Age, Time, Major)

def get_or_none(query):    
    try:
        return query()
    except ObjectDoesNotExist:
        return None

def ages(request):
    template = loader.get_template('ages.html')
    context = { 'ages': Age.objects.all() }
    return HttpResponse(template.render(context, request))

def age(request, age):
    template = loader.get_template('age.html')
    age = Age.objects.get(mini=age)
    context = { 
        'age': age,
        'times': Time.objects.filter(age=age),
        'prev': get_or_none(lambda: Age.objects.get(order=age.order-1)),
        'next': get_or_none(lambda: Age.objects.get(order=age.order+1))
    }
    return HttpResponse(template.render(context, request))

def time(request, age, time):
    template = loader.get_template('time.html')
    time = Time.objects.get(mini=time)
    context = { 
        'time': time,
        'majors': Major.objects.filter(time=time),
        'prev': get_or_none(lambda: Time.objects.get(order=time.order-1)),
        'next': get_or_none(lambda: Time.objects.get(order=time.order+1))
    }
    return HttpResponse(template.render(context, request))