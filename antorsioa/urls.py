from django.urls import (path, re_path)

from . import views

urlpatterns = [
    path('', views.ages, name='ages'),
    re_path('(?P<age>\w+)/(?P<time>\w+)/$', views.time, name='time'),
    re_path('(?P<age>\w+)/$', views.age, name='age'),
]