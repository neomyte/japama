from django.apps import AppConfig


class AntorsioaConfig(AppConfig):
    name = 'antorsioa'
