from django.db import models


class Age(models.Model):
    name = models.CharField(max_length=100)
    mini = models.CharField(max_length=10)
    desc = models.CharField(max_length=1000)
    order = models.IntegerField(unique=True)
    beg = models.CharField(max_length=100)
    end = models.CharField(max_length=100)
    def __str__(self):
        return self.name


class Time(models.Model):
    name = models.CharField(max_length=100)
    end = models.CharField(max_length=100)
    mini = models.CharField(max_length=10)
    desc = models.CharField(max_length=1000)
    order = models.IntegerField(unique=True)
    age = models.ForeignKey(Age, on_delete=models.DO_NOTHING)
    def __str__(self):
        return self.name

class Major(models.Model):
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=1000)
    order = models.IntegerField(unique=True)
    time = models.ForeignKey(Time, on_delete=models.DO_NOTHING)
    def __str__(self):
        return self.name